﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SoltaLabs.Avalon.View.Core.Converters
{
    public class EqualConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
           return int.Parse(value.ToString()) == int.Parse(parameter.ToString());
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool) value)
                return int.Parse(parameter.ToString());
            return null;
        }
    }
}
