﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace SoltaLabs.Avalon.View.Core.Converters
{
    public class EqualToVisibilityRevertConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value.ToString() == parameter.ToString()) return Visibility.Collapsed;
            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
