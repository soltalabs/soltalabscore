﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;
using SoltaLabs.Avalon.Core.Behaviors;
using SoltaLabs.Avalon.View.Core.Interfaces;

namespace SoltaLabs.Avalon.View.Core.UserControls
{
   /// <summary>
   /// Interaction logic for TileGrid.xaml
   /// </summary>
   public partial class TileGrid
   {
      public TileGrid()
      {
         InitializeComponent();

         Loaded += TileGrid_Loaded;
      }

      private void TileGrid_Loaded(object sender, RoutedEventArgs e)
      {
         //NOTE: This is a hack. code behind binding using Tag
         if(TileItems == null) return;
         var contextType = DataContext.GetType();
         foreach (var item in TileItems)
         {
            item.DataContext = contextType.GetProperty(item.Tag + "")?.GetValue(DataContext);
         }
      }

      /// <summary>
      /// Gets or sets additional content for the UserControl
      /// </summary>
      [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
      [Bindable(true)]
      public List<FrameworkElement> TileItems
      {
         get { return (List<FrameworkElement>)GetValue(TileItemsProperty); }
         set { SetValue(TileItemsProperty, value); }
      }

      public static readonly DependencyProperty TileItemsProperty =
          DependencyProperty.Register("TileItems", typeof(List<FrameworkElement>), typeof(TileGrid),
            new PropertyMetadata(new List<FrameworkElement>(), (d, e) =>
            {
            }));

      public FrameworkElement TileMenu
      {
         get { return (FrameworkElement)GetValue(TileMenuProperty); }
         set { SetValue(TileMenuProperty, value); }
      }

      public static readonly DependencyProperty TileMenuProperty =
          DependencyProperty.Register("TileMenu", typeof(FrameworkElement), typeof(TileGrid),
            new PropertyMetadata(null));

      private const int Interval = 5000000;
      private const double FadeOpacity = 0.23;

      public ICommand ActivateContentCommand => new DelegateCommand(ExpandChildView);

      public ICommand HideActiveContentCommand => new DelegateCommand(CloseActive);

      private FrameworkElement _currentActive;

      private void CloseActive(object obj)
      {
         if (_currentActive != null)
         {
            IActiveViewModel sourceViewModel = _currentActive.DataContext as IActiveViewModel;
            sourceViewModel?.OnActivate(null);

            if (_currentActive is IHasBrowser)
               MoveInHideActivateContent();
            else FadeHideActiveContent();
         }
      }

      private void ExpandChildView(object obj)
      {
         int index = int.Parse(obj + "");

         if (TileItems.Count > index)
         {
            FrameworkElement fe = TileItems[index];

            _currentActive = fe;
            IHasBrowser hasBrowser = fe as IHasBrowser;
            if (hasBrowser != null)
            {
               var theBrowser = hasBrowser.Adapter.InitWeb();
               _currentActive = theBrowser;
               MoveInActivateContent(theBrowser);
            }
            else FadeActivateContent(fe);

            IActiveViewModel sourceViewModel = fe.DataContext as IActiveViewModel;
            sourceViewModel?.OnActivate(null);

            CloseButton.IsOpen = true;
         }
      }

      private void FadeActivateContent(FrameworkElement fe)
      {
         AnimateHomePanel(FadeOpacity, 1);

         if (FillGrid.Children.Contains(fe)) return;
         FillGrid.Children.Add(fe);

         DoubleAnimation showAnimation = new DoubleAnimation(0, 1, new Duration(new TimeSpan(Interval)));
         FillGrid.BeginAnimation(OpacityProperty, showAnimation);
      }

      private void FadeHideActiveContent()
      {
         AnimateHomePanel(1, 5);

         DoubleAnimation hideAnimation = new DoubleAnimation(1, 0, new Duration(new TimeSpan(Interval)));
         hideAnimation.Completed += (o, a) =>
         {
            if (_currentActive != null)
               if (FillGrid.Children.Contains(_currentActive))
                  FillGrid.Children.Remove(_currentActive);
         };

         FillGrid.BeginAnimation(OpacityProperty, hideAnimation);
         CloseButton.IsOpen = false;
      }

      private void AnimateHomePanel(double toOpacity, int toZIndex)
      {
         DoubleAnimation hideAnimation = new DoubleAnimation(toOpacity, new Duration(new TimeSpan(Interval)));
         hideAnimation.Completed += (a, b) => Panel.SetZIndex(HomePanel, toZIndex);
         HomePanel.BeginAnimation(OpacityProperty, hideAnimation);
      }

      private void MoveInActivateContent(FrameworkElement fe)
      {
         AnimateHomePanel(FadeOpacity, 1);

         if (FillGrid.Children.Contains(fe)) return;
         FillGrid.Children.Add(fe);

         DoubleAnimation heightAnimation = new DoubleAnimation(0, FillGrid.ActualHeight, new Duration(new TimeSpan(Interval)));
         DoubleAnimation widthAnimation = new DoubleAnimation(0, FillGrid.ActualWidth, new Duration(new TimeSpan(Interval)));
         DoubleAnimation showAnimation = new DoubleAnimation(0, 1, new Duration(new TimeSpan(Interval)));

         FillGrid.BeginAnimation(OpacityProperty, showAnimation);
         fe.BeginAnimation(HeightProperty, heightAnimation);
         fe.BeginAnimation(WidthProperty, widthAnimation);
      }

      private void MoveInHideActivateContent()
      {
         AnimateHomePanel(1, 5);

         DoubleAnimation sizeAnimation = new DoubleAnimation(0, new Duration(new TimeSpan(Interval)));
         sizeAnimation.Completed += (a, b) => FillGrid.Children.Clear();

         if (_currentActive != null)
         {
            _currentActive.BeginAnimation(HeightProperty, sizeAnimation);
            _currentActive.BeginAnimation(WidthProperty, sizeAnimation);
         }
      }

      public void Reset()
      {
         CloseActive(null);
      }
   }
}
