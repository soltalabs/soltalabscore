﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using SoltaLabs.Avalon.Core.Behaviors;
using SoltaLabs.Avalon.View.Core.Interfaces;

namespace SoltaLabs.Avalon.View.Core.UserControls
{
   /// <summary>
   /// Interaction logic for LuckyClover.xaml
   /// </summary>
   public partial class LuckyCloverUC
   {
      public LuckyCloverUC()
      {
         InitializeComponent();

         PreviewMouseLeftButtonDown += LuckyCloverUC_PreviewMouseLeftButtonDown;
         _closeTimer = new DispatcherTimer {Interval = new TimeSpan(0, 0, 30)};
         _closeTimer.Tick += CloseTimer_Tick;
      }

      private void CloseTimer_Tick(object sender, EventArgs e)
      {
         if (_closeCount >= 3)
         {
            HideActiveContentCommand.Execute(null);
         }
         else _closeCount++;
      }

      private void LuckyCloverUC_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
      {
         _closeCount = 0;
      }

      readonly DispatcherTimer _closeTimer;
      private int _closeCount;

      public ICommand ActivateContentCommand => new DelegateCommand(ExpandChildView);

      private FrameworkElement _currentActive;

      private void ExpandChildView(object obj)
      {
         FrameworkElement source = obj as FrameworkElement;
         if (source != null && _currentActive == null)
         {
            _currentActive = source;
            if (_currentActive is IHasBrowser)
               MoveInActivateContent();
            else FadeActivateContent();

            IActiveViewModel sourceViewModel = source.DataContext as IActiveViewModel;
            sourceViewModel?.OnActivate(null);

            //start auto close
            _closeCount = 0;
            _closeTimer.Start();
         }
      }

      public ICommand HideActiveContentCommand => new DelegateCommand(() =>
      {
         IActiveViewModel active = _currentActive.DataContext as IActiveViewModel;

         if (_currentActive is IHasBrowser)
            MoveInHideActivateContent();
         else FadeHideActiveContent();

         active?.OnDeactivate(null);
         _closeTimer.Stop();
      });
   }
}
