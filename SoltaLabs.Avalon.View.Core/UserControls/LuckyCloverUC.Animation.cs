﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using SoltaLabs.Avalon.View.Core.Interfaces;

namespace SoltaLabs.Avalon.View.Core.UserControls
{
   partial class LuckyCloverUC
   {
      private const int Interval = 1000000;
      private void FadeActivateContent()
      {
         if (_currentActive == null) return;
         ContentControl parent = _currentActive.Parent as ContentControl;
         if (parent == null) return; Panel.SetZIndex(parent, 10);

         DoubleAnimation showAnimation = new DoubleAnimation(0, 1, new Duration(new TimeSpan(Interval)));
         DoubleAnimation hideAnimation = new DoubleAnimation(1, 0, new Duration(new TimeSpan(Interval)));

         HomeContent.BeginAnimation(OpacityProperty, hideAnimation);
         CloseButton.IsOpen = true;
         parent.BeginAnimation(OpacityProperty, showAnimation);
      }

      private void FadeHideActiveContent()
      {
         ContentControl parent = _currentActive?.Parent as ContentControl;
         if (parent == null) return;
         Panel.SetZIndex(parent, 3);

         DoubleAnimation showAnimation = new DoubleAnimation(0, 1, new Duration(new TimeSpan(Interval)));
         DoubleAnimation hideAnimation = new DoubleAnimation(1, 0, new Duration(new TimeSpan(Interval)));

         HomeContent.BeginAnimation(OpacityProperty, showAnimation);
         CloseButton.IsOpen = false;
         parent.BeginAnimation(OpacityProperty, hideAnimation);

         _currentActive = null;
      }

      private void MoveInHideActivateContent()
      {
         IHasBrowser webControl = _currentActive as IHasBrowser;
         if (webControl == null) return;
         var theView = webControl.Adapter.GetBrowser();

         ContentControl parent = _currentActive.Parent as ContentControl;
         if (parent == null) return;

         Panel.SetZIndex(parent, 3);

         DoubleAnimation showAnimation = new DoubleAnimation(0, 1, new Duration(new TimeSpan(Interval)));

         HomeContent.BeginAnimation(OpacityProperty, showAnimation);
         theView.Visibility = Visibility.Collapsed;
         CloseButton.IsOpen = false;
         _currentActive = null;
      }

      private void MoveInActivateContent()
      {
         IHasBrowser webControl = _currentActive as IHasBrowser;
         if (webControl == null) return;
         var theView = webControl.Adapter.GetBrowser();
         ContentControl parent = _currentActive.Parent as ContentControl;
         if (parent == null) return;

         Panel.SetZIndex(parent, 10);

         DoubleAnimation heightAnimation = new DoubleAnimation(0, parent.ActualHeight, new Duration(new TimeSpan(5000000)));
         DoubleAnimation widthAnimation = new DoubleAnimation(0, parent.ActualWidth, new Duration(new TimeSpan(5000000)));

         DoubleAnimation hideAnimation = new DoubleAnimation(1, 0, new Duration(new TimeSpan(5000000)));

         HomeContent.BeginAnimation(OpacityProperty, hideAnimation);
         theView.Visibility = Visibility.Visible;
         theView.BeginAnimation(WidthProperty, widthAnimation);
         theView.BeginAnimation(HeightProperty, heightAnimation);
         CloseButton.IsOpen = true;
      }
   }
}
