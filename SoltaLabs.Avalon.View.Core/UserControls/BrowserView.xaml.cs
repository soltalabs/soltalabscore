﻿using System.Windows.Controls;
using SoltaLabs.Avalon.View.Core.Interfaces;

namespace SoltaLabs.Avalon.View.Core.UserControls
{
   /// <summary>
   /// Interaction logic for BrowserView.xaml
   /// </summary>
   public partial class BrowserView : UserControl
   {
      protected WebBrowser _theBrowser;

      public BrowserView()
      {
         _theBrowser = new WebBrowser();
         InitializeComponent();
      }

      public virtual string ConfigKey => string.Empty;

      public WebBrowser InitWeb()
      {
         IWebViewControl webControl = DataContext as IWebViewControl;

         webControl?.InitWeb(_theBrowser, "JourneyMapConfigFile");

         return _theBrowser;
      }
   }
}
