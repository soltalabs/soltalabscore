﻿using System.Windows;

namespace SoltaLabs.Avalon.View.Core.UserControls
{
    partial class LuckyCloverUC
    {
        /// <summary>
        /// Gets or sets additional content for the UserControl
        /// </summary>
        public object TopLeftContent
        {
            get { return GetValue(TopLeftContentProperty); }
            set { SetValue(TopLeftContentProperty, value); }
        }

        public static readonly DependencyProperty TopLeftContentProperty =
            DependencyProperty.Register("TopLeftContent", typeof(object), typeof(LuckyCloverUC),
              new PropertyMetadata(null));

        public object BotLeftContent
        {
            get { return GetValue(BotLeftContentProperty); }
            set { SetValue(BotLeftContentProperty, value); }
        }

        public static readonly DependencyProperty BotLeftContentProperty =
            DependencyProperty.Register("BotLeftContent", typeof(object), typeof(LuckyCloverUC),
              new PropertyMetadata(null));

        public object TopRightContent
        {
            get { return GetValue(TopRightContentProperty); }
            set { SetValue(TopRightContentProperty, value); }
        }

        public static readonly DependencyProperty TopRightContentProperty =
            DependencyProperty.Register("TopRightContent", typeof(object), typeof(LuckyCloverUC),
              new PropertyMetadata(null));

        public object BotRightContent
        {
            get { return GetValue(BotRightContentProperty); }
            set { SetValue(BotRightContentProperty, value); }
        }

        public static readonly DependencyProperty BotRightContentProperty =
            DependencyProperty.Register("BotRightContent", typeof(object), typeof(LuckyCloverUC),
              new PropertyMetadata(null));

        public FrameworkElement HomeContent
        {
            get { return (FrameworkElement)GetValue(HomeContentProperty); }
            set { SetValue(HomeContentProperty, value); }
        }

        public static readonly DependencyProperty HomeContentProperty =
            DependencyProperty.Register("HomeContent", typeof(FrameworkElement), typeof(LuckyCloverUC),
              new PropertyMetadata(null));
    }
}
