﻿using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Input;
using SoltaLabs.Avalon.Core.Behaviors;

namespace SoltaLabs.Avalon.View.Core.UserControls
{
    /// <summary>
    /// Interaction logic for RollPanel.xaml
    /// </summary>
    public partial class RollPanel
    {
        public RollPanel()
        {
            RollCommand = new DelegateCommand(RollDownCallback);
            InitializeComponent();
        }

        private void RollDownCallback(object obj)
        {
            List<Control> parents = new List<Control>();
            if (TopContent?.Parent != null)
                parents.Add((Control) TopContent.Parent);
            if (BodyContent?.Parent != null)
                parents.Add((Control) BodyContent.Parent);
            if (BottomContent?.Parent != null)
                parents.Add((Control) BottomContent.Parent);

            RollContentDown(parents);
        }

       public ICommand RollCommand { get; }
    }
}
