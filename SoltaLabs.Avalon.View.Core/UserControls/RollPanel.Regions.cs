﻿using System.Windows;

namespace SoltaLabs.Avalon.View.Core.UserControls
{
    partial class RollPanel
    {
        /// <summary>
        /// Gets or sets additional contents for the UserControl
        /// </summary>
        public FrameworkElement TopContent
        {
            get { return (FrameworkElement) GetValue(TopContentProperty); }
            set { SetValue(TopContentProperty, value); }
        }

        public static readonly DependencyProperty TopContentProperty =
            DependencyProperty.Register("TopContent", typeof(FrameworkElement), typeof(RollPanel),
              new PropertyMetadata(null));

        public FrameworkElement BodyContent
        {
            get { return (FrameworkElement) GetValue(BodyContentProperty); }
            set { SetValue(BodyContentProperty, value); }
        }

        public static readonly DependencyProperty BodyContentProperty =
            DependencyProperty.Register("BodyContent", typeof(FrameworkElement), typeof(RollPanel),
              new PropertyMetadata(null));

        public FrameworkElement BottomContent
        {
            get { return (FrameworkElement) GetValue(BottomContentProperty); }
            set { SetValue(BottomContentProperty, value); }
        }

        public static readonly DependencyProperty BottomContentProperty =
            DependencyProperty.Register("BottomContent", typeof(FrameworkElement), typeof(RollPanel),
              new PropertyMetadata(null));
    }
}
