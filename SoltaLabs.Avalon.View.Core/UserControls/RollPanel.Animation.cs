﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace SoltaLabs.Avalon.View.Core.UserControls
{
    partial class RollPanel
    {
        private class RollItem
        {
            public Control Obj { get; set; }
            public double NewOffset { get; set; }
        }

        private void RollContentDown(List<Control> parents)
        {
            parents.Sort(Comparer<Control>.Create((e1, e2) => (int) (Canvas.GetTop(e1) - Canvas.GetTop(e2))));
            
            var rollList = parents.Select(p => new RollItem { Obj = p, NewOffset = 0 }).ToList();

            var lastItem = rollList.Last();
            lastItem.NewOffset = 0;
            double stackOffset = lastItem.Obj.ActualHeight;
            //calculate new top offset
            for (int i = 0; i < rollList.Count-1; i++)
            {
                rollList[i].NewOffset = stackOffset;
                stackOffset += rollList[i].Obj.ActualHeight;
            }

            //now roll
            foreach (RollItem rollItem in rollList)
            {
                DoubleAnimation animation = new DoubleAnimation(
                    Canvas.GetTop(rollItem.Obj),
                    rollItem.NewOffset,
                    new Duration(new TimeSpan(0,0,0,1)));   

                rollItem.Obj.BeginAnimation(Canvas.TopProperty, animation);
            }
        }
    }
}