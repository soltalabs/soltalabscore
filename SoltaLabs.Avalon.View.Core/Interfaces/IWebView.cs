﻿using System.Windows.Controls;

namespace SoltaLabs.Avalon.View.Core.Interfaces
{
   public interface IWebView
   {
      WebBrowser GetBrowser();
   }
}
