﻿namespace SoltaLabs.Avalon.View.Core.Interfaces
{
   public interface IActiveViewModel
   {
      void OnActivate(object args);
      void OnDeactivate(object args);
   }
}
