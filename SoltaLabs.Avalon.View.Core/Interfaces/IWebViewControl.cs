﻿using System;
using System.Windows.Controls;

namespace SoltaLabs.Avalon.View.Core.Interfaces
{
   [Obsolete]
   public interface IWebViewControl
   {
      void InitWeb(WebBrowser theBrowser, string webConfigKey);
      WebBrowser GetBrowser();
   }
}
