﻿using SoltaLabs.Avalon.View.Core.Adapters;

namespace SoltaLabs.Avalon.View.Core.Interfaces
{
   public interface IHasBrowser
   {
      string ConfigKey { get; }
      BrowserAdapter Adapter { get; }
   }
}
