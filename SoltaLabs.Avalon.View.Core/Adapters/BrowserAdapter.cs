﻿using System;
using System.Windows.Controls;
using SmartPole.Model;
using SoltaLabs.Avalon.Core.Helper;
using SoltaLabs.Avalon.View.Core.Interfaces;

namespace SoltaLabs.Avalon.View.Core.Adapters
{
   public class BrowserAdapter
   {
      private readonly WebBrowser _theBrowser;

      public BrowserAdapter(IHasBrowser target, WebBrowser browser = null)
      {
         _theBrowser = browser ?? new WebBrowser();

         string webConfigKey = target.ConfigKey;
         var fileName = ConfigurationHelper.GetFileName(webConfigKey);
         var fileContent = ConfigurationHelper.ReadConfigurationContent(fileName);
         _config = ConfigurationHelper.ParseXml<WebBrowserConfig>(fileContent);
      }

      readonly WebBrowserConfig _config;

      public WebBrowser InitWeb()
      {
         if (_theBrowser != null)
         {
            _theBrowser.Navigate(new Uri(_config.StartPage));
            _theBrowser.BindToDomain(_config.Domains);
         }

         return _theBrowser;
      }

      public void OnActivate(object args)
      {
         _theBrowser.Navigate(new Uri(_config.StartPage));
      }

      public void OnDeactivate(object args)
      {
         _theBrowser.Navigate(new Uri(_config.StartPage));
      }

      public WebBrowser GetBrowser()
      {
         return _theBrowser;
      }
   }
}