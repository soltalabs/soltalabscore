﻿using System.Linq;
using System.Xml.Linq;

namespace SoltaLabs.Avalon.Core.Helper
{
   public static class LinqXmlHelper
   {
      public static string GetValue(this XElement element, string elementName)
      {
         var child = element.Element(elementName);
         return child?.Value;
      }

      public static string[] GetValues(this XElement element, string elementName)
      {
         var children = element.Elements(elementName);
         return children.Select(c => c.Value).ToArray();
      }
   }
}
