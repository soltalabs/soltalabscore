﻿using System;
using System.Windows;

namespace SoltaLabs.Avalon.Core.Helper
{
    public static class DispatcherHelper
    {
        /// <summary>
        /// Invoke a delegate at application scope.
        /// </summary>
        /// <param name="delegateMethod"></param>
        public static void InvokeDelegate(Delegate delegateMethod)
        {
            InvokeDelegate(delegateMethod, new object[] { });
        }

        /// <summary>
        /// Invoke a method in UI thread.
        /// </summary>
        /// <param name="delegateMethod"></param>
        /// <param name="param"></param>
        public static void InvokeDelegate(Delegate delegateMethod, object[] param)
        {
            if (Application.Current != null)
                Application.Current.Dispatcher.BeginInvoke(delegateMethod, System.Windows.Threading.DispatcherPriority.Send, param);
        }
    }
}
