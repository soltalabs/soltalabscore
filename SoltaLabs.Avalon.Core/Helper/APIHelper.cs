﻿using System;
using System.IO;
using System.Net;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace SoltaLabs.Avalon.Core.Helper
{
    public static class APIHelper
    {
        public static T GetJsonReponse<T>(string url, NetworkCredential login)
            where T : new()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(url));
            request.Credentials = login;
            try
            {

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    Stream str = response.GetResponseStream();
                    if (str == null)
                        return new T();

                    TextReader reader = new StreamReader(str);
                    string data = reader.ReadToEnd();

                    var jsonObj = JsonConvert.DeserializeObject<T>(data);

                    return jsonObj;
                }
            }
            catch (InvalidOperationException)
            {
                return new T();
            }
        }

        public static T GetXmlReponse<T>(string url, NetworkCredential login = null)
            where T : new()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(url));
            request.Credentials = login;
            try
            {

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    Stream str = response.GetResponseStream();
                    if (str == null)
                        return new T();

                    TextReader reader = new StreamReader(str);

                    XmlSerializer ser = new XmlSerializer(typeof(T));
                    var xmlObj = (T)ser.Deserialize(reader);

                    return xmlObj;
                }
            }
            catch (InvalidOperationException)
            {
                return new T();
            }
        }
    }
}
