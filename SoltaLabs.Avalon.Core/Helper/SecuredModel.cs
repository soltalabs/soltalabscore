﻿using System;
using System.Xml.Linq;

namespace SoltaLabs.Avalon.Core.Helper
{
    public static class SecuredModel
    {
        public static XElement GetCreditCardXml()
        {
            try
            {
                var filename = ConfigurationHelper.GetFileName("SnapperConfigFile");
                string textConfig = ConfigurationHelper.ReadConfigurationContent(filename);
                var config = XElement.Parse(textConfig);
                return config.Element("CreditCard");
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
