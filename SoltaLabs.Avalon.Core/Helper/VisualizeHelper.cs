﻿using System;
using System.Windows;

namespace SoltaLabs.Avalon.Core.Helper
{
    /// <summary>
    /// 
    /// </summary>
    public static class VisualizeHelper
   {
      /// <summary>
      /// Finds the parent.
      /// </summary>
      /// <param name="obj">The obj.</param>
      /// <param name="where">The where.</param>
      /// <returns></returns>
      public static DependencyObject FindParent(this DependencyObject obj, Predicate<DependencyObject> where)
      {
         FrameworkElement frameworkElement = obj as FrameworkElement;
         if (frameworkElement == null)
            return null;
         var parent = frameworkElement.Parent;

         if (parent == null || where(parent))
         {
            return parent;
         }

         return parent.FindParent(where);
      }

      /// <summary>
      /// Finds the type of the parent of.
      /// </summary>
      /// <typeparam name="T"></typeparam>
      /// <param name="obj">The obj.</param>
      /// <returns></returns>
      public static T FindParentOfType<T>(this DependencyObject obj) where T : DependencyObject
      {
         return (T)FindParent(obj, x => x is T);
      }
   }
}