﻿using System;
using System.Configuration;
using System.IO;
using System.Xml.Serialization;

namespace SoltaLabs.Avalon.Core.Helper
{
    public static class ConfigurationHelper
    {
        public static string GetFileName(string key)
        {
            try
            {
                return ConfigurationManager.AppSettings[key];
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        public static string ReadConfigurationContent(string fileName)
        {
            string dir = Path.GetDirectoryName(typeof(ConfigurationHelper).Assembly.Location);

            if (dir != null)
            {
                string fullFile = Path.Combine(dir, fileName);
                if (File.Exists(fullFile))
                {
                    return File.ReadAllText(fullFile);
                }                
            }

            return string.Empty;
        }

        public static T ParseXml<T>(string content)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            TextReader textReader = new StringReader(content);
            return (T)serializer.Deserialize(textReader);
        }
    }
}
