﻿using System;

namespace SoltaLabs.Avalon.Core.Helper
{
   public static class HexHelper
   {
      static readonly char[] HexChars =
      {
         '0','1','2','3',
         '4','5','6','7',
         '8','9','A','B',
         'C','D','E','F'
      };

      public static string ToHex(this byte[] data, int offset, int len)
      {
         char[] a = new char[len << 1];
         int i = 0;

         while (0 != len--)
         {
            byte b = data[offset++];
            a[i++] = HexChars[0xF & (b >> 4)];
            a[i++] = HexChars[0xF & b];
         }

         return new String(a);
      }

      public static byte[] ToAscii(this byte[] data, int offset, int len)
      {
         byte[] a = new byte[len << 1];
         int i = 0;

         while (0 != len--)
         {
            byte b = data[offset++];
            a[i++] = (byte)HexChars[0xF & (b >> 4)];
            a[i++] = (byte)HexChars[0xF & b];
         }

         return a;
      }

      static int Ordinal(char c)
      {
         int i = HexChars.Length;

         while (0 != i--)
         {
            if (HexChars[i] == c) break;
         }

         return i;
      }

      public static byte[] FromAscii(this byte[] d)
      {
         int maxLen = d.Length >> 1;
         byte[] result = new byte[maxLen];
         Boolean isHigh = true;
         int offset = 0;
         byte b = 0;

         foreach (byte c in d)
         {
            int i = Ordinal((char)c);

            if (i >= 0)
            {
               if (isHigh)
               {
                  b = (byte)(i << 4);
               }
               else
               {
                  result[offset++] = (byte)(b | i);
               }

               isHigh = !isHigh;
            }
         }

         if (offset != maxLen)
         {
            byte[] a = new byte[offset];

            Array.Copy(result, 0, a, 0, offset);

            result = a;
         }

         return result;
      }

      public static byte[] FromHex(this string d)
      {
         int maxLen = d.Length >> 1;
         byte[] result = new byte[maxLen];
         Boolean isHigh = true;
         int offset = 0;
         byte b = 0;

         foreach (char c in d)
         {
            int i = Ordinal(c);

            if (i >= 0)
            {
               if (isHigh)
               {
                  b = (byte)(i << 4);
               }
               else
               {
                  result[offset++] = (byte)(b | i);
               }

               isHigh = !isHigh;
            }
         }

         if (offset != maxLen)
         {
            byte[] a = new byte[offset];

            Array.Copy(result, 0, a, 0, offset);

            result = a;
         }

         return result;
      }

      public static string Encode(this byte[] a, int offset, int len)
      {
         return Convert.ToBase64String(a.ToAscii(offset, len));
      }

      public static byte[] Decode(this string res)
      {
         byte[] d = Convert.FromBase64String(res);
         return d.FromAscii();
      }
   }
}
