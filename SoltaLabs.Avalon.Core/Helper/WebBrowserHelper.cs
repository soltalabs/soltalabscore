﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;

namespace SoltaLabs.Avalon.Core.Helper
{
   public static class WebBrowserHelper
   {
      public static void BindToDomain(this WebBrowser theBrowser, IList<string> domainList)
      {
         theBrowser.Navigating += (sender, args) =>
         {
            if (domainList.All(domain => args.Uri.Host != domain))
            {
               args.Cancel = true;
               if (theBrowser.CanGoBack)
                  theBrowser.GoBack();
            }
         };


      }
   }
}