﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Input;

namespace SoltaLabs.Avalon.Core.Utilities
{
   /// <summary>
   /// 
   /// </summary>
   [ComVisible(false), System.Security.SuppressUnmanagedCodeSecurity()]
   public class KbHookManager : IDisposable
   {
      #region constants

      private const int WH_KEYBOARD_LL = 13;
      private const int WM_KEYUP = 0x0101;
      private const int WM_SYSKEYUP = 0x0105;

      #endregion

      private readonly HookHandlerDelegate _handler;
      private readonly IntPtr _hookId = IntPtr.Zero;

      /// <summary>
      /// Initializes a new instance of the <see cref="KeyboardHookManager"/> class.
      /// </summary>
      public KbHookManager()
      {
         //regis the handler
         _handler = new HookHandlerDelegate(HookCallback);

         //get the Id for hooking
         using (Process curProcess = Process.GetCurrentProcess())
         using (ProcessModule curModule = curProcess.MainModule)
         {
            _hookId = SetWindowsHookEx(WH_KEYBOARD_LL, _handler,
                GetModuleHandle(curModule.ModuleName), 0);
         }

      }

      private IntPtr HookCallback(int nCode, IntPtr wParam, ref KBDLLHOOKSTRUCT lParam)
      {
         if (nCode == 0)
         {
            if (((lParam._vkCode == 0x09) && (lParam._flags == 0x20)) ||  // Alt+Tab
               ((lParam._vkCode == 0x1B) && (lParam._flags == 0x20)) ||      // Alt+Esc
               ((lParam._vkCode == 0x1B) && (lParam._flags == 0x00)) ||      // Ctrl+Esc
               ((lParam._vkCode == 0x5B) && (lParam._flags == 0x01)) ||      // Left Windows Key
               ((lParam._vkCode == 0x5C) && (lParam._flags == 0x01)) ||      // Right Windows Key
               ((lParam._vkCode == 0x73) && (lParam._flags == 0x20)) ||      // Alt+F4
               ((lParam._vkCode == 0x20) && (lParam._flags == 0x20))
               )        // Crt+Alt+Del can not be stopped
            {
               OnKeyIntercepted(new KeyboardHookEventArgs(lParam._vkCode, lParam._flags, false));
               return new IntPtr(1);
            }
         }
         return CallNextHookEx(_hookId, nCode, wParam, ref lParam);
      }

      #region Native methods

      internal struct KBDLLHOOKSTRUCT
      {
         public int _vkCode;
         int _scanCode;
         public int _flags;
         int _time;
         int _dwExtraInfo;
      }

      internal delegate IntPtr HookHandlerDelegate(int nCode, IntPtr wParam, ref KBDLLHOOKSTRUCT lParam);

      [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
      internal static extern IntPtr GetModuleHandle(string lpModuleName);

      [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
      internal static extern IntPtr SetWindowsHookEx(int idHook,
          HookHandlerDelegate lpfn, IntPtr hMod, uint dwThreadId);

      [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
      [return: MarshalAs(UnmanagedType.Bool)]
      internal static extern bool UnhookWindowsHookEx(IntPtr hhk);

      [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
      internal static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode,
          IntPtr wParam, ref KBDLLHOOKSTRUCT lParam);

      [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true, CallingConvention = CallingConvention.Winapi)]
      internal static extern short GetKeyState(int keyCode);

      #endregion

      #region IDisposable Members

      /// <summary>
      /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
      /// </summary>
      public void Dispose()
      {
         UnhookWindowsHookEx(_hookId);
      }

      #endregion

      #region Notify Out

      /// <summary>
      /// Event triggered when a keystroke is intercepted by the 
      /// low-level hook.
      /// </summary>
      public event KeyboardHookEventHandler KeyIntercepted;

      /// <summary>
      /// Raises the KeyIntercepted event.
      /// </summary>
      /// <param name="e">An instance of KeyboardHookEventArgs</param>
      public void OnKeyIntercepted(KeyboardHookEventArgs e)
      {
         KeyIntercepted?.Invoke(e);
      }

      /// <summary>
      /// Delegate for KeyboardHook event handling.
      /// </summary>
      /// <param name="e">An instance of InterceptKeysEventArgs.</param>
      public delegate void KeyboardHookEventHandler(KeyboardHookEventArgs e);

      /// <summary>
      /// Event arguments for the KeyboardHook class's KeyIntercepted event.
      /// </summary>
      public class KeyboardHookEventArgs : EventArgs
      {
         private readonly string _keyName;
         private readonly int _keyCode;
         private readonly int _keyFlag;
         private readonly bool _passThrough;

         /// <summary>
         /// The name of the key that was pressed.
         /// </summary>
         public string KeyName
         {
            get { return _keyName; }
         }

         /// <summary>
         /// The virtual key code of the key that was pressed.
         /// </summary>
         public int KeyCode
         {
            get { return _keyCode; }
         }

         /// <summary>
         /// Gets the key flag.
         /// </summary>
         /// <value>The key flag.</value>
         public int KeyFlag
         {
            get { return _keyFlag; }
         }

         /// <summary>
         /// True if this key combination was passed to other applications,
         /// false if it was trapped.
         /// </summary>
         public bool PassThrough
         {
            get { return _passThrough; }
         }

         /// <summary>
         /// Initializes a new instance of the <see cref="KeyboardHookEventArgs"/> class.
         /// </summary>
         /// <param name="evtKeyCode">The evt key code.</param>
         /// <param name="keyFlag">The key flag.</param>
         /// <param name="evtPassThrough">if set to <c>true</c> [evt pass through].</param>
         public KeyboardHookEventArgs(int evtKeyCode, int keyFlag, bool evtPassThrough)
         {
            _keyName = ((Key)evtKeyCode).ToString();
            _keyCode = evtKeyCode;
            _keyFlag = keyFlag;
            _passThrough = evtPassThrough;
         }
      }

      #endregion
   }
}
