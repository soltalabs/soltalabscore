﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace SoltaLabs.Avalon.Core.Utilities
{
   public static class ObservableCollectionEx
   {
      /// <summary>
      /// Adds the range.
      /// </summary>
      /// <typeparam name="T"></typeparam>
      /// <param name="collection">The collection.</param>
      /// <param name="items">The items.</param>
      public static void AddRange<T>(this ObservableCollection<T> collection, IEnumerable<T> items)
      {
         foreach (var item in items)
         {
            collection.Add(item);
         }
      }

      public static void Dummy()
      {
      }
   }
}
