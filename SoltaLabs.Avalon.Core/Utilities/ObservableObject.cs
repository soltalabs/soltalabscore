﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;

namespace SoltaLabs.Avalon.Core.Utilities
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T">itself</typeparam>
    [Serializable]
    public class ObservableObject<T> : INotifyPropertyChanged
    //where T : ObservableObject<T>
    {
        /// <summary>
        /// 
        /// </summary>
        private PropertyChangedEventHandler _propertyChanged;

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged
        {
            add { _propertyChanged += value; }
            remove { _propertyChanged -= value; }
        }

        /// <summary>
        /// Creates the args.
        /// </summary>
        /// <param name="propertyExpression">The property expression.</param>
        /// <returns></returns>
        protected static PropertyChangedEventArgs CreateArgs(
            Expression<Func<T, object>> propertyExpression)
        {
            var lambda = propertyExpression as LambdaExpression;
            MemberExpression memberExpression = null;
            if (lambda.Body is UnaryExpression)
            {
                var unaryExpression = lambda.Body as UnaryExpression;
                if (unaryExpression != null) memberExpression = unaryExpression.Operand as MemberExpression;
            }
            else
            {
                memberExpression = lambda.Body as MemberExpression;
            }

            if (memberExpression != null)
            {
                var propertyInfo = memberExpression.Member as PropertyInfo;

                if (propertyInfo != null) return new PropertyChangedEventArgs(propertyInfo.Name);
            }
            return new PropertyChangedEventArgs(string.Empty);
        }

        /// <summary>
        /// Notifies the change.
        /// </summary>
        /// <param name="args">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        protected void NotifyChange(PropertyChangedEventArgs args)
        {
           _propertyChanged?.Invoke(this, args);
        }

        protected void Set<TP>(ref TP field, TP value, Expression<Func<T, object>> propertyExpression)
        {
            if (!Equals(field, value))
            {
                field = value;
                NotifyChange(CreateArgs(propertyExpression));
            }
        }
    }
}
