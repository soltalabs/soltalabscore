﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;
using SoltaLabs.Avalon.Core.Behaviors;
using SoltaLabs.Avalon.Core.Helper;

namespace SoltaLabs.Avalon.Core.Utilities
{
   /// <summary>
   /// 
   /// </summary>
   public static class FullScreenManager
   {
      /// <summary>
      /// Goes the fullscreen.
      /// </summary>
      /// <param name="window">The window.</param>
      public static void GoFullscreen(this Window window)
      {
         // Make window borderless
         window.WindowStyle = WindowStyle.None;
         window.ResizeMode = ResizeMode.NoResize;
         window.Topmost = true;

         window.WindowState = WindowState.Maximized;

         //mark it fullscreen
         SetIsFullScreen(window, true);
      }

      /// <summary>
      /// Exits the full screen.
      /// </summary>
      /// <param name="window">The window.</param>
      public static void ExitFullScreen(this Window window)
      {
         window.WindowStyle = WindowStyle.SingleBorderWindow;
         window.WindowState = WindowState.Normal;
         window.Topmost = false;
         SetIsFullScreen(window, false);
      }

      #region API

      // Nearest monitor to window
      const int MonitorDefaultToNearest = 2;

      // To get a handle to the specified monitor
      [DllImport("user32.dll")]
      static extern IntPtr MonitorFromWindow(IntPtr hwnd, int dwFlags);

      // To get the working area of the specified monitor
      [DllImport("user32.dll")]
      internal static extern bool GetMonitorInfo(HandleRef hmonitor, [In, Out] MONITORINFOEX monitorInfo);

      // Rectangle (used by MONITORINFOEX)
      [StructLayout(LayoutKind.Sequential)]
      internal struct RECT
      {
         public int Left;
         public int Top;
         public int Right;
         public int Bottom;
      }

      // Monitor information (used by GetMonitorInfo())
      [StructLayout(LayoutKind.Sequential)]
      internal class MONITORINFOEX
      {
         public int cbSize;
         public RECT rcMonitor; // Total area
         public RECT rcWork; // Working area
         public int dwFlags;
         [MarshalAs(UnmanagedType.ByValArray, SizeConst = 0x20)]
         public char[] szDevice;
      }

      #endregion

      #region IsFullScreen Dependency Property

      /// <summary>
      /// 
      /// </summary>
      public static DependencyProperty IsFullScreenProperty =
         DependencyProperty.RegisterAttached(
         "IsFullScreen",
         typeof(bool),
         typeof(FullScreenManager),
         new PropertyMetadata(false));

      /// <summary>
      /// Sets the is full screen.
      /// </summary>
      /// <param name="target">The target.</param>
      /// <param name="value">if set to <c>true</c> [value].</param>
      public static void SetIsFullScreen(DependencyObject target, bool value)
      {
         target.SetValue(IsFullScreenProperty, value);
      }

      /// <summary>
      /// Gets the is full screen.
      /// </summary>
      /// <param name="target">The target.</param>
      /// <returns></returns>
      public static bool GetIsFullScreen(DependencyObject target)
      {
         return (bool)target.GetValue(IsFullScreenProperty);
      }

      #endregion

      #region Base Screen Solution Dependency Property

      /// <summary>
      /// 
      /// </summary>
      public static DependencyProperty BaseScreenSolutionProperty =
         DependencyProperty.RegisterAttached(
         "BaseScreenSolution",
         typeof(Point),
         typeof(FullScreenManager),
         new PropertyMetadata(new Point(1024, 728)));

      /// <summary>
      /// Sets the default screen solution.
      /// </summary>
      /// <param name="target">The target.</param>
      /// <param name="value">The value.</param>
      public static void SetBaseScreenSolution(DependencyObject target, Point value)
      {
         target.SetValue(BaseScreenSolutionProperty, value);
      }

      /// <summary>
      /// Gets the default screen solution.
      /// </summary>
      /// <param name="target">The target.</param>
      /// <returns></returns>
      public static Point GetBaseScreenSolution(DependencyObject target)
      {
         return (Point)target.GetValue(BaseScreenSolutionProperty);
      }

      #endregion

      #region IsAutoMaximizedPanel Dependency Property

      /// <summary>
      /// Indicate that the main Panel shoul auto expand to the maximun size by the monitor information
      /// </summary>
      public static DependencyProperty IsAutoMaximizedPanelProperty =
         DependencyProperty.RegisterAttached(
            "IsAutoMaximizedPanel",
            typeof(bool),
            typeof(FullScreenManager),
            new PropertyMetadata(false, AutoMaximizedIndicatorChangedCallback));

      /// <summary>
      /// Autoes the maximized indicator changed callback.
      /// </summary>
      /// <param name="d">The d.</param>
      /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
      private static void AutoMaximizedIndicatorChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
      {
         Panel panel = d as Panel;
         bool isMax = (bool) e.NewValue;
         if (panel != null && isMax)
         {
            Point baseRes = GetBaseScreenSolution(panel);
            Point monitor = GetScreenResolution();

            if (monitor.X != 0 && monitor.Y != 0)
            {
               Window main = Application.Current.MainWindow;
               //Window main = panel.FindParentOfType<Window>();
               //panel.w
               if (main != null)
               {
                  main.Width = monitor.X;
                  main.Height = monitor.Y;
                  main.Left = 0;
                  main.Top = 0;
                  double scaleX = monitor.X/panel.Width;
                  double scaleY = monitor.Y/panel.Height;

                  double scale = scaleX < scaleY ? scaleX : scaleY;

                  Layout.SetSpotZoomScale(panel, (float) scale);
               }
            }
         }
      }

      /// <summary>
      /// Sets the is auto maximized panel.
      /// </summary>
      /// <param name="target">The target.</param>
      /// <param name="value">if set to <c>true</c> [value].</param>
      public static void SetIsAutoMaximizedPanel(DependencyObject target, bool value)
      {
         target.SetValue(IsAutoMaximizedPanelProperty, value);
      }

      /// <summary>
      /// Gets the is auto maximized panel.
      /// </summary>
      /// <param name="target">The target.</param>
      /// <returns></returns>
      public static bool GetIsAutoMaximizedPanel(DependencyObject target)
      {
         return (bool)target.GetValue(IsAutoMaximizedPanelProperty);
      }

      #endregion

      /// <summary>
      /// Gets the size of the monitor.
      /// </summary>
      /// <param name="panel">The panel.</param>
      /// <returns></returns>
      public static Point GetMonitorSize(this Panel panel)
      {
         // Get handle for nearest monitor to this window
         Window window = panel.FindParentOfType<Window>();
         if (window == null) return new Point(0, 0);

         WindowInteropHelper wih = new WindowInteropHelper(window);
         IntPtr hMonitor = MonitorFromWindow(wih.Handle, MonitorDefaultToNearest);

         // Get monitor info
         MONITORINFOEX monitorInfo = new MONITORINFOEX();
         monitorInfo.cbSize = Marshal.SizeOf(monitorInfo);
         GetMonitorInfo(new HandleRef(window, hMonitor), monitorInfo);

         // Create working area dimensions, converted to DPI-independent values
         HwndSource source = HwndSource.FromHwnd(wih.Handle);
         if (source == null) return new Point(0, 0); // Should never be null
         if (source.CompositionTarget == null) return new Point(0, 0); // Should never be null
         Matrix matrix = source.CompositionTarget.TransformFromDevice;
         RECT workingArea = monitorInfo.rcMonitor;
         Point dpiIndependentSize =
             matrix.Transform(
             new Point(
                 workingArea.Right - workingArea.Left,
                 workingArea.Bottom - workingArea.Top));

         return dpiIndependentSize;
      }

      /// <summary>
      /// Gets the size of the monitor.
      /// </summary>
      /// <returns></returns>
      public static Point GetScreenResolution()
      {
         return new Point(
            System.Windows.SystemParameters.PrimaryScreenWidth,
            System.Windows.SystemParameters.PrimaryScreenHeight);
      }
   }
}
