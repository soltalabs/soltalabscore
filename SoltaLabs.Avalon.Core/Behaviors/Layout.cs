﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

/*
 * Main Container must be a canvas
 */
namespace SoltaLabs.Avalon.Core.Behaviors
{
   public class Layout
   {
      private const string ScaleName = "SmartPole";

      #region Scale Group

      public static DependencyProperty ScaleGroupProperty =
         DependencyProperty.RegisterAttached(
            "ScaleGroup",
            typeof(string),
            typeof(Layout),
            new PropertyMetadata(string.Empty));

      public static void SetScaleGroup(DependencyObject targer, string name)
      {
         targer.SetValue(ScaleGroupProperty, name);
      }

      public static string GetScaleGroup(DependencyObject targer)
      {
         return (string)targer.GetValue(ScaleGroupProperty);
      }

      #endregion

      public static DependencyProperty SpotZoomScaleProperty =
         DependencyProperty.RegisterAttached(
            "SpotZoomScale",
            typeof(float),
            typeof(Layout),
            new PropertyMetadata(1.0f, ScaleChangedCallback));

      public static void SetSpotZoomScale(DependencyObject target, float scale)
      {
         target.SetValue(SpotZoomScaleProperty, scale);
      }

      public static float GetSpotZoomScale(DependencyObject target)
      {
         return (float)target.GetValue(SpotZoomScaleProperty);
      }

      private static void ScaleChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
      {
         var panel = d as Panel;
         float newValue = (float)e.NewValue;
         float oldValue = (float)e.OldValue;

         //change if only new scale value is different
         if (panel != null && newValue != oldValue)
         {
            TransformGroup group = panel.RenderTransform as TransformGroup;
            if (group != null)
            {
               //remove old items
               var customedItems = group.Children.Where(x => GetScaleGroup(x) == ScaleName);
               foreach (var customedItem in customedItems)
               {
                  group.Children.Remove(customedItem);
               }
            }
            else
            {
               //if no groud define yet, create one
               group = new TransformGroup();
               panel.RenderTransform = group;
            }

            Point afterScale = new Point(panel.Width * newValue, panel.Height * newValue);

            // add a scale transform
            ScaleTransform scale = new ScaleTransform { ScaleX = newValue, ScaleY = newValue };
            SetScaleGroup(scale, ScaleName);

            group.Children.Add(scale);


            Window main = Application.Current.MainWindow;
            if (main != null)
            {
               //add a move tranform to match to center point
               TranslateTransform translate = new TranslateTransform
                                                 {
                                                    X = (main.Width - afterScale.X) / 2,
                                                 };

               //we only take the X to make to center horizontal align, regardless vertical

               SetScaleGroup(translate, ScaleName);

               group.Children.Add(translate);
               panel.HorizontalAlignment = HorizontalAlignment.Left;
               panel.VerticalAlignment = VerticalAlignment.Top;
               var marginTop =  (main.Height - afterScale.Y)/2;
               panel.Margin = new Thickness(0,marginTop, 0, marginTop);
            }
         }
      }
   }
}
