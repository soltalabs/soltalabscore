﻿using System.Windows;
using System.Windows.Input;

namespace SoltaLabs.Avalon.Core.Behaviors
{
    public class MouseBehavior
    {
        #region MouseClick

        /// <summary>
        /// Attached Command Dependency property for TextChanged event
        /// </summary>
        public static DependencyProperty MouseClickCommandProperty =
           DependencyProperty.RegisterAttached("MouseClickCommand",
                                               typeof(ICommand), typeof(MouseBehavior),
                                               new PropertyMetadata(null, MouseClickCallback));
        /// <summary>
        /// Attached Command Parameter Dependency property for TextChanged event
        /// </summary>
        public static DependencyProperty MouseClickCommandParameterProperty =
            DependencyProperty.RegisterAttached("MouseClickCommandParameter",
                                                typeof(object), typeof(MouseBehavior),
                                                new PropertyMetadata(null));
        /// <summary>
        /// TextBoxBases the change callback.
        /// </summary>
        /// <param name="obj">The obj.</param>
        /// <param name="args">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void MouseClickCallback(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            FrameworkElement uiElement = obj as FrameworkElement;
            ICommand command = args.NewValue as ICommand;

            if (uiElement != null)
                if (command != null)
                    uiElement.MouseUp += UiElementOnMouseUp;
                else
                    uiElement.MouseUp -= UiElementOnMouseUp;
        }

        private static void UiElementOnMouseUp(object sender, MouseButtonEventArgs mouseButtonEventArgs)
        {
            FrameworkElement uiElement = sender as FrameworkElement;
            if (uiElement == null) return;

            ICommand command = GetMouseClickCommand(uiElement);
            if (command != null)
            {
                object param = GetMouseClickCommandParameter(uiElement);
                command.Execute(new[] { param, mouseButtonEventArgs });
            }
        }

        /// <summary>
        /// Gets the text changed command.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <returns></returns>
        public static ICommand GetMouseClickCommand(DependencyObject target)
        {
            return (ICommand)target.GetValue(MouseClickCommandProperty);
        }

        /// <summary>
        /// Gets the text changed command parameter.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <returns></returns>
        public static object GetMouseClickCommandParameter(DependencyObject target)
        {
            return target.GetValue(MouseClickCommandParameterProperty);
        }

        /// <summary>
        /// Sets the text changed command parameter.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="value">The value.</param>
        public static void SetMouseClickCommandParameter(DependencyObject target, object value)
        {
            target.SetValue(MouseClickCommandParameterProperty, value);
        }

        /// <summary>
        /// Sets the text changed command.
        /// </summary>
        /// <param name="target">The target.</param>
        /// <param name="value">The value.</param>
        public static void SetMouseClickCommand(DependencyObject target, ICommand value)
        {
            target.SetValue(MouseClickCommandProperty, value);
        }

        #endregion
    }
}
