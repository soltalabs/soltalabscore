﻿namespace SoltaLabs.Avalon.Core.Behaviors
{
   public interface IContentResetable
   {
      void Reset();
   }
}
