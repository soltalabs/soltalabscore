﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace SoltaLabs.Avalon.Core.Behaviors
{
   public class VisibilityBehavior
   {
      /// <summary>
      /// Attached Command Dependency property for TextChanged event
      /// </summary>
      public static DependencyProperty AutoHideTimeoutProperty =
         DependencyProperty.RegisterAttached("MouseClickCommand",
                                             typeof(int), typeof(MouseBehavior),
                                             new PropertyMetadata(0, AutoHideTimeoutCallback));

      private static void AutoHideTimeoutCallback(DependencyObject target, DependencyPropertyChangedEventArgs e)
      {
         int newValue = (int)e.NewValue;
         if (newValue > 0)
         {
            Control fe = target as Control;
            if (fe != null)
            {
               fe.IsVisibleChanged += (sender, vE) =>
               {
                  if ((Visibility)vE.NewValue == Visibility.Visible)
                  {
                     DispatcherTimer timer = new DispatcherTimer
                     {
                        Interval = new TimeSpan(0, 0, newValue)
                     };
                     timer.Tick += (obj, arg) =>
                     {
                        fe.Visibility = Visibility.Collapsed;
                        timer.Stop();
                     };

                     timer.Start();
                  }
               };               
            }
         }
      }

      public static int GetAutoHideTimeoutProperty(DependencyObject target)
      {
         return (int) target.GetValue(AutoHideTimeoutProperty);
      }

      public static void SetAutoHideTimeoutProperty(DependencyObject target, int value)
      {
         target.SetValue(AutoHideTimeoutProperty, value);
      }
   }
}
