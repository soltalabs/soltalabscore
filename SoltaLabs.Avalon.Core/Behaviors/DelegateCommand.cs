﻿using System;
using System.Windows.Input;

namespace SoltaLabs.Avalon.Core.Behaviors
{
   /// <summary>
   /// 
   /// </summary>
   public class DelegateCommand : ICommand
   {
      private readonly Action<object> _excute;
      private readonly Predicate<object> _canExcute;

      /// <summary>
      /// Initializes a new instance of the <see cref="DelegateCommand"/> class.
      /// </summary>
      /// <param name="excute">The excute.</param>
      public DelegateCommand(Action<object> excute)
         : this(excute, x => true)
      {
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="DelegateCommand"/> class.
      /// </summary>
      /// <param name="excute">The excute.</param>
      /// <param name="canExcute">The can excute.</param>
      public DelegateCommand(Action<object> excute, Predicate<object> canExcute)
      {
         _excute = excute;
         _canExcute = canExcute;
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="DelegateCommand"/> class.
      /// </summary>
      /// <param name="excute">The excute.</param>
      public DelegateCommand(Action excute)
         : this(excute, x => true)
      {
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="DelegateCommand"/> class.
      /// </summary>
      /// <param name="excute">The excute.</param>
      /// <param name="canExcute">The can excute.</param>
      public DelegateCommand(Action excute, Predicate<object> canExcute)
      {
         _excute = x => excute();
         _canExcute = canExcute;
      }

      #region ICommand Members

      /// <summary>
      /// Defines the method that determines whether the command can execute in its current state.
      /// </summary>
      /// <param name="parameter">Data used by the command.  If the command does not require data to be passed, this object can be set to null.</param>
      /// <returns>
      /// true if this command can be executed; otherwise, false.
      /// </returns>
      public bool CanExecute(object parameter)
      {
         return _canExcute(parameter);
      }

      /// <summary>
      /// Occurs when changes occur that affect whether or not the command should execute.
      /// </summary>
      public event EventHandler CanExecuteChanged;

      /// <summary>
      /// Defines the method to be called when the command is invoked.
      /// </summary>
      /// <param name="parameter">Data used by the command.  If the command does not require data to be passed, this object can be set to null.</param>
      public void Execute(object parameter)
      {
         _excute(parameter);
      }

      #endregion
   }

   public class DelegateCommand<T> : ICommand
      where T : class
   {
      private readonly Action<T> _excute;
      private readonly Predicate<T> _canExcute;

      /// <summary>
      /// Initializes a new instance of the <see cref="DelegateCommand"/> class.
      /// </summary>
      /// <param name="excute">The excute.</param>
      public DelegateCommand(Action<T> excute)
         : this(excute, x => true)
      {
         _excute = excute;
      }

      /// <summary>
      /// Initializes a new instance of the <see cref="DelegateCommand"/> class.
      /// </summary>
      /// <param name="excute">The excute.</param>
      /// <param name="canExcute">The can excute.</param>
      public DelegateCommand(Action<T> excute, Predicate<T> canExcute)
      {
         _excute = excute;
         _canExcute = canExcute;
      }

      #region ICommand Members

      /// <summary>
      /// Defines the method that determines whether the command can execute in its current state.
      /// </summary>
      /// <param name="parameter">Data used by the command.  If the command does not require data to be passed, this object can be set to null.</param>
      /// <returns>
      /// true if this command can be executed; otherwise, false.
      /// </returns>
      public bool CanExecute(object parameter)
      {
         if (typeof(T) == parameter.GetType())
            return _canExcute((T)parameter);
         return false;
      }

      /// <summary>
      /// Occurs when changes occur that affect whether or not the command should execute.
      /// </summary>
      public event EventHandler CanExecuteChanged;

      /// <summary>
      /// Defines the method to be called when the command is invoked.
      /// </summary>
      /// <param name="parameter">Data used by the command.  If the command does not require data to be passed, this object can be set to null.</param>
      public void Execute(object parameter)
      {
         if (typeof(T) == parameter.GetType())
            _excute((T)parameter);
         else
            _excute(null);
      }

      #endregion
   }
}