﻿using System;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Threading;

namespace SoltaLabs.Avalon.Core.Behaviors
{
   public class IdleBehavior
   {
      private const int Tick = 5;

      public static DependencyProperty IsAutoResetProperty =
         DependencyProperty.RegisterAttached("IsAutoReset",
                                       typeof(bool), typeof(IdleBehavior),
                                       new PropertyMetadata(false, SetupIdle));

      public static bool GetIsAutoReset(DependencyObject target)
      {
         return (bool)target.GetValue(IsAutoResetProperty);
      }

      public static void SetIsAutoReset(DependencyObject target, bool value)
      {
         target.SetValue(IsAutoResetProperty, value);
      }

      private static void SetupIdle(DependencyObject d, DependencyPropertyChangedEventArgs e)
      {
         FrameworkElement fe = d as FrameworkElement;
         if (fe == null) return;
         IIdleTarget idleTarget = d as IIdleTarget;
         if (idleTarget == null) return;

         fe.Unloaded += (unloader, args) =>
         {
            if (idleTarget.Runner != null && idleTarget.Runner.IsEnabled)
            {
               idleTarget.Runner.Stop();
            }
         };

         if (idleTarget.Runner == null)
         {
            idleTarget.Runner = new DispatcherTimer();
            idleTarget.Runner.Interval = new TimeSpan(0, 0, Tick);
            idleTarget.Runner.Tick += (sender, arg) =>
            {
               if (idleTarget.CurrentIdleInSecond >= idleTarget.TimeoutInSecond)
               {
                  idleTarget.Runner?.Stop();
                  idleTarget.Reset();
               }
               else idleTarget.CurrentIdleInSecond += Tick;
            };
         }

         fe.PreviewMouseLeftButtonDown += (sender, erg) =>
         {
            if(idleTarget.Runner != null && !idleTarget.Runner.IsEnabled)
               idleTarget.Runner.Start();
            idleTarget.CurrentIdleInSecond = 0;
         };
      }
   }

   public interface IIdleTarget : IContentResetable
   {
      int TimeoutInSecond { get; }
      int CurrentIdleInSecond { get; set; }
      DispatcherTimer Runner { get; set; }
   }
}
