﻿using System;

namespace SoltaLabs.Avalon.Core.Attributes
{
   public class TargetAttribute : Attribute
   {
      public TargetAttribute(Targets target)
      {
         Target = target;
      }

      public Targets Target { get; }
   }
}
